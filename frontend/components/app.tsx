import * as React from 'react';
import { Container, Accordion, Card } from 'react-bootstrap';
import { About } from './About';

export class App extends React.Component<{}, {}> {
	render() {
		return (
			<Container>
				<Accordion defaultActiveKey="0">
					<Card>
						<Accordion.Toggle as={Card.Header} eventKey="0">
							<h2>About Me</h2>
						</Accordion.Toggle>
						<Accordion.Collapse eventKey="0">
							<Card.Body style={{backgroundColor: "#75777a"}}>
								<About />
							</Card.Body>
						</Accordion.Collapse>
					</Card>
					<Card>
						<Accordion.Toggle as={Card.Header} eventKey="1">
							Projects
						</Accordion.Toggle>
						<Accordion.Collapse eventKey="1">
							<Card.Body>
								#Project Content!
							</Card.Body>
						</Accordion.Collapse>
					</Card>
				</Accordion>
			</Container>
		);
	}
}
