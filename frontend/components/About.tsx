import * as React from 'react';
import { Container, Row, Col, Card } from 'react-bootstrap';

export function About() {
	// Stateless Component for the 'About Me' section of the front page.
	
	// style for the cards displayed
	const genericCardStyle = {
		backgroundColor: '#2d3b51',
		color: 'white',
		padding: '5%',
	}

	// rendering the view.
	return (
		<Container>
			<Row>
				<Col lg={true}>
					<Card style={genericCardStyle}>
						<h3>Peter Vlasveld</h3>	
		<img 
			className="img-responsive center" 
			src="/images/me.jpg"
			style={{borderRadius: '50%', width: '100%', height: 'auto', display: 'block'}}
		/>
					</Card>
				</Col>
				<Col lg={true}>
						<Row>
							<Card style={genericCardStyle}>
								<p><b>Current position</b>: Junior Developer, Cyclica</p>
								<p><b>Location</b>: Toronto, ON, Canada</p>
								<p><b>Education</b>:</p>
								<p>Bioinformatics Graduate Certificate, Seneca College</p>
								<p>H.B.Sc Neuroscience & Philosophy, University of Toronto</p>
								<p>Computer Engineering Technology Diploma, Algonquin College</p>
								<p><br/><b>"Principled design and implementation is the name of the game."</b></p>		
							</Card>
						</Row>
						<Row>
							<Card style={genericCardStyle}>
								<h4>Primary Skillset</h4>
								<p><b>Backend</b>: SQL, Postgres,	Python, Django, Rust, Rocket</p>
								<p><b>Frontend</b>: Typescript, Javascript, ReactJS, Redux, NPM</p> 
							</Card>
						</Row>
						<Row>
							<Card style={genericCardStyle}>
								<Row>
									<Col style={{display: 'inline-block', textAlign: 'center'}}>
										<a 
											style={{color: 'white'}} 
											href="https://gitlab.com/fizzizist"
										>
											<i 
												style={{fontSize: '40px'}} 
												className="fab fa-gitlab"
											/>
									</a>
								</Col>
								<Col style={{display: 'inline-block', textAlign: 'center'}}>
									<a 
										style={{color: 'white', fontSize: '40px'}} 
										href="https://www.linkedin.com/in/peter-vlasveld-19756459/"
									>
										<i className="fab fa-linkedin"/>
									</a>
								</Col>
							</Row>
							<Row style={{marginTop: '15px'}}>	
								<p><b>Email</b>: pvlasveld@mailfence.com</p>
							</Row>
						</Card>
					</Row>	
				</Col>
			</Row>
		</Container>
	);
}
